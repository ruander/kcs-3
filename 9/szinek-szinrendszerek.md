#Színek színrendszerek

**A szintaktika a kiírásokhoz a markdown alapján van (md kiterjesztés)**

[Leírás](https://www.markdownguide.org/cheat-sheet/)

###számrendszerek
2-es számrendszer
- értékkészlet :0,1
- helyiértékek: ...,2^1,2^0

8 bites érték: 01010101 

0|10 -> 00000000|2
255|10 -> 11111111|2

10-es számrendszer
- értékkészlet :0,1,...,9
- helyiértékek: ...,10^1,10^0

RGB szín 2es számrendszerben

000000001111111100000000

10es ben:
rgb(0,255,0)

16os számrendszer:
- értékkészlet :0,1,...,9,A(10),B(11),C(12),D(13),E(14),F(15) 
- helyiértékek: ...,16^1,16^0

16osban:```#00ff00```

ezt így nem lehet: #ff3345 -> #f345

így igen: #aa44dd -> ```#a4d```

```
#000 -> fekete
#fff -> fehér
ha egyezik a 3 érték akkor vmilyen szürke
#RRGGBB
#f00 -> piros
#0f0 -> zöld
#00f -> kék
#ff0 -> sárga
#0ff -> cyan
#f0f -> magenta
#eea -> #eeeeaa -> rgb(238,238,170) 
